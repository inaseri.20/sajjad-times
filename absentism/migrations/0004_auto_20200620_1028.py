# Generated by Django 3.0.4 on 2020-06-20 05:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('absentism', '0003_auto_20200620_0954'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='endDate',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='startDate',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
