from django.apps import AppConfig


class AbsentismConfig(AppConfig):
    name = 'absentism'
